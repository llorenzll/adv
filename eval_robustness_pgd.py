import torch
import torchvision
import torchvision.transforms as transforms
from argparse import ArgumentParser
from resnet import *  # Import your ResNet models
from advertorch.attacks import PGDAttack, L2PGDAttack, LinfPGDAttack

# Argument parsing
parser = ArgumentParser(description='Evaluate model adversarial robustness on CIFAR-10')
parser.add_argument('--state', type=str, required=True, help='Path to the model checkpoint file')
parser.add_argument('--attack', type=str, required=True, choices=['pgd', 'l2pgd', 'linfpgd'], help='Attack type: pgd, l2pgd, or linfpgd')
args = parser.parse_args()

# Data transformation
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

# Data loading
testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=100, shuffle=False, num_workers=4)

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Model loading
model = ResNet50() if 'teacher' in args.state else ResNet18()
model = model.to(device)
model.load_state_dict(torch.load(args.state))
model.eval()

# Selecting the attack
if args.attack == 'pgd':
    print("Attack: PGD")
    adversary = PGDAttack(model, loss_fn=torch.nn.CrossEntropyLoss(reduction="sum"), eps=8/255, nb_iter=40, eps_iter=0.01, rand_init=True, clip_min=0.0, clip_max=1.0, targeted=False)
elif args.attack == 'l2pgd':
    print("Attack: L2PGD")
    adversary = L2PGDAttack(model, loss_fn=torch.nn.CrossEntropyLoss(reduction="sum"), eps=1.0, nb_iter=40, eps_iter=0.01, rand_init=True, clip_min=0.0, clip_max=1.0, targeted=False)
elif args.attack == 'linfpgd':
    print("Attack: LinfPGD")
    adversary = LinfPGDAttack(model, loss_fn=torch.nn.CrossEntropyLoss(reduction="sum"), eps=8/255, nb_iter=40, eps_iter=0.01, rand_init=True, clip_min=0.0, clip_max=1.0, targeted=False)

# Evaluation
correct = 0
total = 0
print("Using device:", 'CPU' if not torch.cuda.is_available() else torch.cuda.get_device_name())

for inputs, targets in testloader:
    inputs, targets = inputs.to(device), targets.to(device)
    adv_inputs = adversary.perturb(inputs, targets)

    # Evaluate model performance on adversarial examples
    outputs = model(adv_inputs)
    _, predicted = outputs.max(1)
    total += targets.size(0)
    correct += predicted.eq(targets).sum().item()

# Results
print('Adversarial Accuracy: {:.2f}%'.format(100 * correct / total))
