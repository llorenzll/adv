import torch
import torchvision
import torchvision.transforms as transforms
from argparse import ArgumentParser
from autoattack import AutoAttack
from resnet import *

parser = ArgumentParser(description='Evaluate model adversarial robustness on CIFAR-10')
parser.add_argument('--state', type=str, help='Path to the model checkpoint file (must be provided)')
args = parser.parse_args()

transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

testset = torchvision.datasets.CIFAR10(root='./data', train=False, download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=100, shuffle=False, num_workers=4)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


model = ResNet50() if 'teacher' in args.state else ResNet18()
model = model.to(device)
model.load_state_dict(torch.load(args.state))
model.eval()

adversary = AutoAttack(model, norm='Linf', eps=8/255, version='standard')

correct = 0
total = 0

print("Using device:", 'CPU' if not torch.cuda.is_available() else torch.cuda.get_device_name())

for inputs, targets in testloader:
    inputs, targets = inputs.to(device), targets.to(device)
    adv_inputs = adversary.run_standard_evaluation(inputs, targets, bs=100)

    # Evaluate model performance on adversarial examples
    outputs = model(adv_inputs)
    _, predicted = outputs.max(1)
    total += targets.size(0)
    correct += predicted.eq(targets).sum().item()

print('Adversarial Accuracy: {:.2f}%'.format(100 * correct / total))

