echo "Step 1/6: Eval teacher with normal cifar10"
python eval_robustness_pgd.py --state SL/weights/SL_teacher_cifar10_resnet50_30th_epoch.pth --attack pgd
wait

echo "Step 2/6: Eval normal cifar10 student"
python eval_robustness_pgd.py --state SL/weights/SL_student_cifar10_resnet18_30th_epoch.pth --attack pgd
wait

echo "Step 3/6: Eval teacher with robust cifar10"
python eval_robustness_pgd.py --state SL/weights/SL_teacher_robust-cifar10_resnet50_30th_epoch.pth --attack pgd
wait

echo "Step 4/6: Eval robust cifar10 student"
python eval_robustness_pgd.py --state SL/weights/SL_student_robust-cifar10_resnet18_30th_epoch.pth --attack pgd
wait

echo "Step 5/6: Eval teacher with non-robust cifar10"
python eval_robustness_pgd.py --state SL/weights/SL_teacher_non-robust-cifar10_resnet50_30th_epoch.pth --attack pgd
wait

echo "Step 6/6: Eval non-robust cifar10 student"
python eval_robustness_pgd.py --state SL/weights/SL_student_non-robust-cifar10_resnet18_30th_epoch.pth --attack pgd
wait