import torch, os
import numpy as np
from torchvision.datasets import CIFAR10
from resnet import *
from argparse import ArgumentParser
from torchvision import transforms
from torch.utils.data import DataLoader
from dataset import *

parser = ArgumentParser()
parser.add_argument('--dataset', default='cifar10', choices=['cifar10', 'robust-cifar10', 'non-robust-cifar10'])
parser.add_argument('--save', default=True, action='store_true')
parser.add_argument('--model', type=str, default='resnet18')
parser.add_argument('--epoch', type=int, default=30)
parser.add_argument('--teacher_weight', type=str, default='./weights/SL_cifar10_resnet50_30.pth')
parser.add_argument('--temp', type=int, default=20)
parser.add_argument('--distil_weight', type=float, default=0.7)
args = parser.parse_args()

# Logging
if not os.path.exists('./log'):
    os.makedirs('./log')

log = open(f'./log/KDStudent_{args.dataset}_{args.model}_{args.epoch}.txt', 'w')

def print_and_log(text):
    print(text)
    log.write(text + '\n')
    log.flush()

print_and_log(f'Start training SL on dataset {args.dataset}')

# Data
transform_train = transforms.Compose([
    transforms.RandomCrop(32, padding=4),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
])
transform_test = transforms.Compose([
    transforms.ToTensor()
])
train_set = CIFAR10('../data', train=True, transform=transform_train, download=True)
test_set = CIFAR10('../data', train=False, transform=transform_test, download=True)
if 'robust' in args.dataset:
    train_set = Crafted_CIFAR10_Training_Set(None, args.dataset)
train_loader = DataLoader(train_set, shuffle=False, batch_size=128, num_workers=8)
test_loader = DataLoader(test_set, shuffle=False, batch_size=128, num_workers=8)

# Model
# Teacher
teacher_model = ResNet50().cuda()
teacher_model.load_state_dict(torch.load(args.teacher_weight))
teacher_model.eval()

if args.model == 'resnet18':
    model = ResNet18().cuda()
elif args.model == 'resnet50':
    model = ResNet18().cuda()

loss_fn = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9, weight_decay=5e-4)
scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=args.epoch, eta_min=0)

print_and_log(f'KDStudent, Dataset = {args.dataset}, Teacher = {args.teacher_weight}, Student = {args.model}, Epoch = {args.epoch}, Temp = {args.temp}, Distil Weight = {args.distil_weight}')

# Modify from orginal code.
# Training
print_and_log("Using device: " + torch.cuda.get_device_name())
Acc = []
for i in range(1, args.epoch + 1):
    model.train()
    for data, label in train_loader:
        data, label = data.cuda(), label.cuda()
        logits = model(data)
        soft_label = F.softmax(teacher_model(data) / args.temp, dim=1)
        out = model(data)
        soft_out = F.softmax(out / args.temp, dim=1)
        loss = (1 - args.distil_weight) * F.cross_entropy(out,label) + (args.distil_weight) * loss_fn(soft_label,soft_out)
        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 5.0)
        optimizer.step()
        
    total_correct = 0
    model.eval()
    with torch.no_grad():
        for data, label in test_loader:
            data, label = data.cuda(), label.cuda()
            logits = model(data)
            total_correct += (torch.argmax(logits, axis=1) == label).sum().detach().item()
    test_acc = total_correct/10000
    scheduler.step()
    print_and_log(f'SL, epoch {i}, Dataset = {args.dataset}, Acc = {test_acc}')
    Acc.append(test_acc)

    if args.save and i == args.epoch:
    # save weights with model name, dataset name, and training epoch
    # check if weights folder exists
        if not os.path.exists('./weights'):
            os.makedirs('./weights')
        torch.save(model.state_dict(), f'./weights/SL_student_{args.dataset}_{args.model}_{i}th_epoch.pth')
        print_and_log('Saved: ' + f'./weights/SL_student_{args.dataset}_{args.model}_{i}th_epoch.pth')

log.close()